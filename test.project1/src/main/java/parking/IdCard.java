package parking;

public class IdCard {
    private int id;
    private String name;
    private String jabatan;
    private int salary;
    private int parkingId;

    public void showData() {
        System.out.println("IDParking: " + parkingId);
        System.out.println("NAME: " + name + "\n");


        if (getJabatan().equals("Karyawan")) {
            salary = 3000000 * getIdCard();
        } else {
            salary = 10000000 * getIdCard();
        }

        System.out.println("NAME: " + name);
        System.out.println("SALARY: " + salary + "\n");

    }

    public void setData(int id, String name, String jabatan, int parkingId) {
        this.jabatan = jabatan;
        this.id = id;
        this.name = name;
        this.parkingId = parkingId;
    }

    public int getIdCard() {
        return id;
    }

    public String getJabatan() {
        return jabatan;
    }
}

